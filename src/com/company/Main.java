package com.company;

public class Main {
    public static class Mahasiswa {
        String nama;
        int nim;
        String prodi;
        int tahunmasuk;
        Transkrip[] transkrip;
    }

    public static class Kursus {
        String nama;
        int kodematkul;
        int sks;
    }

    public static class Transkrip {
        Mahasiswa mhs;
        Kursus kursus;
        int nilai;
    }

    public static void addTranscript(Mahasiswa mahasiswa, Transkrip transkrip, Transkrip[] transkrips) {
        Transkrip[] transkripBaru = new Transkrip[transkrips.length + 1];
        for (int i = 0; i < transkrips.length; i++) {
            transkripBaru[i] = transkrips[i];
        }
        transkripBaru[transkrips.length] = transkrip;
        mahasiswa.transkrip = transkripBaru;
    }

    public static void title(Mahasiswa mhs, Transkrip[] transkrips) {
        System.out.println("Transkrip nilai untuk " + mhs.nama + ":");
        for (Transkrip transcript : transkrips) {
            if (transcript != null && transcript.mhs == mhs) {
                cetakTranscript(transcript);
            }
        }
    }

    public static void cetakTranscript(Transkrip transcript) {
        System.out.println(transcript.kursus.nama + ": " + transcript.nilai);
    }

    public static void main(String[] args) {
        Mahasiswa mhs1 = new Mahasiswa();
        mhs1.nama = "Nur Shine Di Saviola";
        mhs1.nim = 225014;
        mhs1.prodi = "Teknologi Informasi";
        mhs1.tahunmasuk = 2022;
        mhs1.transkrip = new Transkrip[0];
        // LENGKAPI CODE DISINI (5)

        Mahasiswa mhs2 = new Mahasiswa();
        mhs2.nama = "Ayato Kamisato";
        mhs2.nim = 215018;
        mhs2.prodi = "Teknologi Informasi";
        mhs2.tahunmasuk = 2021;
        mhs2.transkrip = new Transkrip[1];
        // LENGKAPI CODE DISINI (2.5)

        Kursus mk1 = new Kursus();
        mk1.kodematkul = 1;
        mk1.nama = "Pemrograman Lanjut";
        mk1.sks = 5;
        // LENGKAPI CODE DISINI (5)

        Kursus mk2 = new Kursus();
        mk2.kodematkul = 2;
        mk2.nama = "Desain Dasar Antarmuka Pengguna";
        mk2.sks = 2;
        // LENGKAPI CODE DISINI (2.5)

        Transkrip[] transcripts = new Transkrip[4];

        Transkrip t1 = new Transkrip();
        t1.kursus = mk1;
        t1.mhs = mhs1;
        t1.nilai = 90;
        // LENGKAPI CODE DISINI (7.5)

        Transkrip t2 = new Transkrip();
        t2.kursus = mk2;
        t2.mhs = mhs1;
        t2.nilai = 92;
        // LENGKAPI CODE DISINI (2.5)

        Transkrip t3 = new Transkrip();
        t3.kursus = mk1;
        t3.mhs = mhs2;
        t3.nilai = 89;
        // LENGKAPI CODE DISINI (2.5)

        Transkrip t4 = new Transkrip();
        t4.kursus = mk2;
        t4.mhs = mhs2;
        t4.nilai = 94;
        // LENGKAPI CODE DISINI (2.5)

        addTranscript(mhs1, t1, mhs1.transkrip);
        addTranscript(mhs1, t2, mhs1.transkrip);
        title(mhs1, mhs1.transkrip);
        addTranscript(mhs2, t3, mhs2.transkrip);
        addTranscript(mhs2, t4, mhs2.transkrip);
        title(mhs2, mhs2.transkrip);
    }
}